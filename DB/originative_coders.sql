-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2017 at 07:54 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `originative_coders`
--
CREATE DATABASE IF NOT EXISTS `originative_coders` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `originative_coders`;

-- --------------------------------------------------------

--
-- Table structure for table `create_profile`
--

CREATE TABLE `create_profile` (
  `id` int(20) NOT NULL,
  `post_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `building_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sit_available` int(3) NOT NULL,
  `room_number` int(3) NOT NULL,
  `mobile_nimber` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `typ` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `conditi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `create_profile`
--

INSERT INTO `create_profile` (`id`, `post_id`, `user_name`, `building_name`, `location`, `district`, `sit_available`, `room_number`, `mobile_nimber`, `typ`, `conditi`, `is_trashed`) VALUES
(1, '2', 'rakib uddin', 'idris  villa ', 'halishahar k-block', 'chittagong', 2, 101, '01816356287', 'male', 'per person sit fee 2k\r\nneed NID/Birth certificate', 'NO'),
(2, '3', 'Tanvir uddin', 'amrna bhobon', 'halishahar,k-block', 'chittagong', 2, 103, '01845451234', 'male', 'fee 3k\r\nneed NID/Birth Certificate', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `hostels`
--

CREATE TABLE `hostels` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `addr` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `t_seats` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `a_seats` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pic` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hostels`
--

INSERT INTO `hostels` (`id`, `name`, `addr`, `phone`, `type`, `t_seats`, `a_seats`, `pic`) VALUES
(1, 'mm', 'jhakjs', '0000000', 'Nresidential', '', '', ''),
(2, 'new', 'hjjka nsns', '0189990099', 'Nresidential', '', '', ''),
(3, 'new', 'hjhkjhk', '01999299', 'Nresidential', '', '', '1507266522'),
(4, 'mkl', 'hjk', '019992993', 'Nresidential', '', '', '1507266617'),
(5, 'nnn', 'hjjjjjjjhh', '12333333', 'residential', '', '', '1507268505'),
(6, 'nnn', 'hjjjjjjjhh', '12333333', 'residential', '', '', '1507269234'),
(7, 'nnn', 'hjjjjjjjhh', '12333333', 'Nresidential', '', '', '1507269753'),
(8, 'nnn', 'hjjjjjjjhh', '12333333', 'Nresidential', '', '', '1507269835'),
(9, 'nnn', 'hjjjjjjjhh', '12333333', 'Nresidential', '', '', '15072708906.jpg'),
(10, 'gg', 'gggggggggggggggggggggg', '1111111111', 'residential', '1233', '123', '15072719056.jpg'),
(11, 'gg', 'gggggggggggggggggggggg', '1111111111', 'residential', '1233', '123', '15072719222.jpg'),
(12, 'chatrabash', 'Chittagong', '112344433', 'residential', '123', '3', '1507301190Screenshot (1).png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `create_profile`
--
ALTER TABLE `create_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_id` (`post_id`);

--
-- Indexes for table `hostels`
--
ALTER TABLE `hostels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `create_profile`
--
ALTER TABLE `create_profile`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hostels`
--
ALTER TABLE `hostels`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
