<?php
namespace App\Hostels;

use App\Message\Message;
use App\Model\Database;
use PDO;


class Hostels extends Database
{
    public $id, $name, $addr, $pic ;


    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray))
            $this->id = $postArray["id"];

        if (array_key_exists("name", $postArray))
            $this->name = $postArray["name"];

        if (array_key_exists("addr", $postArray))
            $this->addr = $postArray["addr"];


        if (array_key_exists("pic", $postArray))
            $this->pic = $postArray["pic"];


    }// end of setData Method


    public function store()
    {

        $sqlQuery = "INSERT INTO hostels ( name, addr, pic ) VALUES ( ?, ?, ?)";
        $sth = $this->dbh->prepare($sqlQuery);


        $dataArray = [$this->name, $this->addr,  $this->pic];

        $status = $sth->execute($dataArray);

        if ($status)

            Message::message("Success! Data has been inserted successfully<br>");
        else
            Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method

    public function index(){

        $sqlQuery = "SELECT * FROM hostels ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }// end of index() Method


}