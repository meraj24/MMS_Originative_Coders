<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Hostel Registration </title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/normalize.css">
    <link rel="stylesheet" href="../../../resources/style.css">
</head>
<body style="background-color: rgba(70,82,24,0.62)">
<!--Navigation Starts-->
<div>
    <nav class="navbar navbar-default navbar-fixed-top mynav" style="background-color: black">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../../../index.php" style="margin-left: 40px;'"><b style="font-size: 55px; color: #985f0d;margin-top: 3px">H</b>OSTELS ONLINE</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php"> HOSTELS  <span class="sr-only">(current)</span></a></li>
                    <li><a href=""> RESERVATION S</a></li>
                    <li><a href=""> FEATURES </a></li>
                    <li><a href=""> ABOUT US</a></li>
                    <li><a href=""> CONTACT US </a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<!--Navigation Ends-->
<div class="container">
    <div class="hostel-info-area" style="background-color:transparent ;text-align: justify; border-radius: 10px">
        <h1> Hostel Registration Form </h1>


<form action="store.php" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label for="name">Hostal Name</label>
        <input type="text" class="form-control" name="name" placeholder="Enter Hostal Name Here...">
    </div>
    <div class="form-group" >
        <label for="addr">Hostal Address</label>
        <input style="height: 100px" type="text" class="form-control" name="addr" placeholder="Enter Hostel Address Here...">
    </div>

    <div class="form-group">
        <label for="phone"> Contact Number </label>
        <input type="number" class="form-control" name="phone" placeholder="Enter Hostel Address Here...">
    </div>
    <div class="form-group">
        <label class="control-label">Hostel Type</label>
        <br><br>
        <input type="radio" name="type" id="residential" value="residential" >
        <label for="residential">Residential</label>
        <br>
        <input type="radio" name="type" id="Nresidential" value="Nresidential">
        <label for="Nresidential">Non - residential</label>
    </div>
    <div class="form-group">
        <label for="t_seats"> Total Seats </label>
        <input type="number" class="form-control" name="t_seats" placeholder="Total Seats" >
    </div>
    <div class="form-group">
        <label for="a_seats"> Available Seats </label>
        <input type="number" class="form-control" name="a_seats" placeholder="Available Seats">
    </div>
    <div class="form-group">
        <label for="pic"> Picture </label>
        <input type="file" class="" name="File2Upload" required="">
    </div>

    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
    </div>
</div>

</body>
</html>